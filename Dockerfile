FROM node:14

RUN mkdir -p /usr/src/app/backend

WORKDIR /usr/src/app/backend

# COPY REQUIRED FILES
COPY package.json .
COPY . .

# INSTALL DEPENDENCIES
RUN yarn install


EXPOSE 8080

CMD ["yarn", "dev"]
