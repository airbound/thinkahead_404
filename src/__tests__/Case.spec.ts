import { Case } from '../entities/Case';
test("getCase should return a case instance", () => {
    expect(Case.getCase({id: "test", isTaken: false, number: 0, x: 0, y: 0})).toBeDefined();
})

test("setTaken should set isTaken to true ", () => {
    const gridCase = new Case("test", 0, false, 0, 0)
    gridCase.setTaken()
    expect(gridCase.isTaken).toBeTruthy();
})

test("setTaken 2 time should set isTaken to true ", () => {
    const gridCase = new Case("test", 0, false, 0, 0)
    gridCase.setTaken()
    gridCase.setTaken()
    expect(gridCase.isTaken).toBeTruthy();
})