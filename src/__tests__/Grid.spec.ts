import { exception } from "console";
import { Case } from "../entities/Case";
import { Grid } from "../entities/Grid"
import { generateGrid } from '../utils/generateGrid';

test("Should set the right lastCase", () => {
    const generatedGrid = generateGrid()
    const grid = new Grid(generatedGrid, undefined, false)
    const exepctedId = generatedGrid[0][0].id;
    grid.setLastCase(exepctedId);
    expect(grid.lastCase?.id).toBe(exepctedId)
});

test("Selectcase Should mark the case as selected", () => {
    const generatedGrid = generateGrid()
    const grid = new Grid(generatedGrid, undefined, false)
    const exepctedId = generatedGrid[0][0].id;
    grid.selectCase(exepctedId);
    expect(grid.getCase(exepctedId)?.isTaken).toBeTruthy()
});

test("Selectcase Should mark the case as selected", () => {
    const generatedGrid = generateGrid()
    const grid = new Grid(generatedGrid, undefined, false)
    const exepctedId = generatedGrid[1][2].id;
    grid.selectCase(generatedGrid[1][2].id);
    expect(grid.getCase(exepctedId)?.isTaken).toBeTruthy(); 
});

test("Selectcase Should not mark the case as selected", () => {
    const generatedGrid = generateGrid()
    const grid = new Grid(generatedGrid, undefined, false)
    const exepctedId = generatedGrid[1][2].id;
    grid.selectCase(generatedGrid[3][1].id);
    expect(() => grid.selectCase(exepctedId)).toThrow(); 
});

test("checkFinish should return false", () => {
    const generatedGrid = generateGrid()
    const grid = new Grid(generatedGrid, undefined, false)
    expect(grid.checkFinish()).toBeFalsy()
})

test("checkFinish should return true", () => {
    const finishGrid = [
        [new Case("a", 1, true, 0, 0), new Case("a", 1, true, 0, 1), new Case("a", 1, true, 0, 2), new Case("a", 1, true, 0, 3)],
        [new Case("a", 1, false, 1, 0), new Case("a", 1, false, 1, 1), new Case("a", 1, false, 1, 2), new Case("a", 1, false, 1, 3)]
    ]
    const grid = new Grid(finishGrid, undefined, false)
    expect(grid.checkFinish()).toBeTruthy()
})

test("checkFinish should return true", () => {
    const finishGrid = [
        [new Case("a", 1, true, 0, 0), new Case("a", 1, false, 0, 1), new Case("a", 1, false, 0, 2), new Case("a", 1, false, 0, 3)],
        [new Case("a", 1, true, 1, 0), new Case("a", 1, false, 1, 1), new Case("a", 1, false, 1, 2), new Case("a", 1, false, 1, 3)]
    ]
    const grid = new Grid(finishGrid, undefined, false)
    expect(grid.checkFinish()).toBeTruthy()
})




