import { Player } from '../entities/Player';
test("incrementTime should increment player time", () =>{
    const player = new Player("test", 0, 0);
    player.incrementTime()
    expect(player.time).toBe(1)
});

test("incrementTime should work with positive number", () =>{
    const player = new Player("test", 0, 0);
    player.addScore(10)
    expect(player.score).toBe(10)
})

test("incrementTime should work with negative number", () =>{
    const player = new Player("test", 0, 0);
    player.addScore(-10)
    expect(player.score).toBe(-10)
})

test("incrementTime should work with negative and positive number", () =>{
    const player = new Player("test", 0, 0);
    player.addScore(-10)
    player.addScore(10)
    expect(player.score).toBe(0)
})