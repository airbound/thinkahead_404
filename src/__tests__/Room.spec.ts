import { Grid } from '../entities/Grid';
import { Player } from '../entities/Player';
import { Room } from '../entities/Room';

test("increment turn should add +1 to turn numbers", () => {
    const room = new Room("test", null, 0, [], new Grid([[]], undefined, false))
    room.incrementTurn();
    expect(room.turnsNumber).toBe(1)
})

test("Should set to the next player", () => {
    const room = new Room("test", null, 0, [new Player("p1", 0, 0), new Player("p2", 0, 0)], new Grid([[]], undefined, false))
    room.setCurrentPlayer()
    expect(room.currentPlayer?.nickname).toBe("p1")
})

test("Should set to the next player", () => {
    const room = new Room("test", null, 0, [new Player("p1", 0, 0), new Player("p2", 0, 0)], new Grid([[]], undefined, false))
    room.setCurrentPlayer()
    room.incrementTurn();
    room.setCurrentPlayer()
    expect(room.currentPlayer?.nickname).toBe("p2")
})