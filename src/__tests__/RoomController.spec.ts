import { RoomController } from './../controllers/RoomControllers';
import { RoomRepository } from '../repositories/RoomRepository';


test("It should create Room", async() => {
    const roomController = new RoomController();
    const roomCode = roomController.create();
    const storedRoom = RoomRepository.find(roomCode)
    expect(storedRoom).toBeDefined()
});

test("It add a player to the Room", () => {
    const roomController = new RoomController();
    const roomCode = roomController.create();
    roomController.join("testPlayer", roomCode)
    const storedRoom = RoomRepository.find(roomCode)
    expect(storedRoom?.players?.length).toBe(1)
});

test("play should not throw and player should have score", () => {
    const roomController = new RoomController();
    const roomCode = roomController.create();
    roomController.join("testPlayer1", roomCode)
    roomController.join("testPlayer2", roomCode)
    const storedRoom = RoomRepository.find(roomCode)
    const caseId = storedRoom?.grid.cases[0][0].id
    const score = storedRoom?.grid.cases[0][0].number
    roomController.play("testPlayer1", roomCode, caseId!);
    expect(storedRoom?.players[0].score).toBe(score)
});

