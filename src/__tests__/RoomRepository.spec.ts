import { Grid } from "../entities/Grid";
import { Room } from "../entities/Room";
import { RoomRepository } from "../repositories/RoomRepository";



test("should add a room", async() => {
    RoomRepository.add(new Room("test", null, 0, [], new Grid([[]], undefined, false)));
    const room = RoomRepository.find("test")
    expect(room?.code).toBe("test")
})



