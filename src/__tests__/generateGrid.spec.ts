import { generateGrid } from '../utils/generateGrid';
test("It should generate a 10 row grid", () => {
    const grid = generateGrid()
    expect(grid.length).toBe(10)
})

test("It should generate a 10 col grid", () => {
    const grid = generateGrid()
    expect(grid[0].length).toBe(10)
    expect(grid[1].length).toBe(10)
    expect(grid[2].length).toBe(10)
    expect(grid[3].length).toBe(10)
    expect(grid[4].length).toBe(10)
    expect(grid[5].length).toBe(10)
    expect(grid[6].length).toBe(10)
    expect(grid[7].length).toBe(10)
    expect(grid[8].length).toBe(10)
    expect(grid[9].length).toBe(10)
})