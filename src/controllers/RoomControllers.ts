import { IRoom } from "../interfaces/IRoom";
import { v4 } from "uuid";
import { generateGrid } from "../utils/generateGrid";
import { Room } from "../entities/Room";
import { RoomRepository } from '../repositories/RoomRepository';
import { Player } from "../entities/Player";
import { Grid } from "../entities/Grid";
export class RoomController {
  public create(): string {
    const room = new Room(v4(), null, 0, [],new Grid(generateGrid(), undefined, false));
    RoomRepository.add(room)
    //emit socket created
    return room.code;
  }
  public async join(playerName: string, roomCode: string) {
    const room: Room | undefined =  RoomRepository.find(roomCode);
    if(!room) {
      throw new Error("Room don't exist");
    }
    if (room.players.length === 2) {
      throw new Error("Room Full");
    }
    const player = new Player(playerName, 0, 0)
    if(room.players.length === 0) {
      room.currentPlayer === player;
    }
    room.addPlayer(player)
  }

  public play(playerName: string, roomCode: string, caseId: string) {
    const room: Room | undefined =  RoomRepository.find(roomCode);
    if(!room) {
      throw new Error("Room not found")
    }
    const player = room.players[room.turnsNumber % 2];
    if(!player) {
      throw new Error("Player not in the game")

    }
    if(room.players[room.turnsNumber % 2]?.nickname !== playerName) {
      throw new Error("This is not your turn :)")
    }
    try {
      const selectedCase = room.grid.selectCase(caseId);
      player.addScore(selectedCase.number)
      room.incrementTurn()
      room.setCurrentPlayer()
    } catch (error) {
      throw new Error(error)
    }
  }

  public hasWinner(roomCode: string): boolean {
    const room: Room | undefined =  RoomRepository.find(roomCode);
    if(!room) {
      throw new Error("Room not found")
    }
    return room.grid.checkFinish();
  }
}
