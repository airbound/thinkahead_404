import { ICase } from '../interfaces/ICase';
export class Case implements ICase {
    id: string;
    number: number;
    isTaken: boolean;
    x: number;
    y: number;
    
    constructor(id: string, number: number, isTaken: boolean, x: number, y: number) {
        this.id = id
        this.number = number
        this.isTaken = isTaken
        this.x = number;
        this.y = number;
    }

    static getCase(data: ICase): Case {
        const {id , number, isTaken, x, y} = data;
        return new Case(id , number, isTaken, x, y);
    }

    public setTaken() {
        if(this.isTaken) return;
        this.isTaken = true;
    }
}
