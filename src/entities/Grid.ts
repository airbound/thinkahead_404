import { ICase } from '../interfaces/ICase';
import { IGrid } from '../interfaces/IGrid';
import { Case } from './Case';
export class Grid implements IGrid {
    cases: Array<Array<ICase>>;
    lastCase: ICase | null | undefined;
    isVertical: boolean;
    constructor(cases: Array<Array<ICase>>, lastCase: Case | null | undefined, isVertical: boolean) {
        this.cases = cases;
        this.lastCase = lastCase
        this.isVertical = isVertical;
    }
    public getCase(id: string) {
        const flatGrid = this.cases.reduce((acc, val) => [ ...acc, ...val ])
        return flatGrid.find( i => i.id === id)
    }
    public setLastCase(id: string) {
       this.lastCase = this.getCase(id)
    }

    public selectCase(id: string): ICase {
        const selectedCase = this.getCase(id);
        if(!selectedCase) {
            throw new Error("Case not found")
        };
        const gridCase = Case.getCase(selectedCase);
        if(!this.lastCase) {
            gridCase.setTaken()
            this.cases = this.cases.map(row => row.map(col => col.id === id ? gridCase : col))
            this.lastCase = selectedCase;
            this.isVertical = !this.isVertical;
            return selectedCase
        }
        if(this.isVertical && gridCase.y !== this.lastCase.y || !this.isVertical && gridCase.y) {
            throw new Error("This case can not be selected")
        } 
        gridCase.setTaken()
        this.cases = this.cases.map(row => row.map(col => col.id === id ? gridCase : col))
        this.lastCase = selectedCase;
        this.isVertical = !this.isVertical;
        return selectedCase
    }
    public checkFinish() {
        const reduced = this.cases.reduce((acc: any, row) => {
            row.map(col => {
                acc.x[col.x] = [...(acc.x[col.x] || []), col.isTaken]
                acc.y[col.y] =  [...(acc.y[col.y] || []), col.isTaken]
            })
            return acc;
        },{x:{}, y:{}})
        
        const isOneRowEmpty = Object.keys(reduced.x).map(o => reduced.x[o].includes(true)).includes(true)
        const isOneColEmpty = Object.keys(reduced.y).map(o => reduced.y[o].includes(true)).includes(true)
        return isOneRowEmpty || isOneColEmpty;
    
    }
}
