import { IPlayer } from '../interfaces/IPlayer';
export class Player implements IPlayer {
    nickname: string;
    score: number;
    time: number;

    constructor(nickname: string, score: number, time: number) {
        this.nickname = nickname;
        this.score = score;
        this.time = time;
    }

    public incrementTime() {
        this.time = this.time + 1;
    }

    public addScore(score: number) {
        this.score = this.score + score;
    }
}
    