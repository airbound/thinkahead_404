import { IGrid } from '../interfaces/IGrid';
import { IRoom } from '../interfaces/IRoom';
import {IPlayer} from '../interfaces/IPlayer'
import { Player } from './Player';
import { Grid } from './Grid';
export class Room implements IRoom {
    code: string;
    currentPlayer: Player | null;
    turnsNumber: number;
    players: Player[];
    grid: Grid;

    constructor(code: string, currentPlayer: Player | null, turnsNumber: number, players: Player[], grid: Grid) {
        this.code = code;
        this.currentPlayer = currentPlayer;
        this.turnsNumber = turnsNumber;
        this.players = players;
        this.grid = grid;
    }

    public incrementTurn() {
        this.turnsNumber = this.turnsNumber + 1;
    }
    
    public addPlayer(player: Player) {
        this.players = [...this.players, player];
    }

    public setCurrentPlayer() {
        if(!this.currentPlayer) {
            this.currentPlayer = this.players[0];
            return;
        }
        const currentPlayerIndex = this.players.map(player => player.nickname).findIndex(playerName => playerName === this.currentPlayer?.nickname);
        this.currentPlayer = this.players[(currentPlayerIndex + 1) % 2]
    }

    


}
