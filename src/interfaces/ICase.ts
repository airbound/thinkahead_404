export interface ICase {
    id: string
    number: number
    isTaken: boolean
    x: number
    y: number
}