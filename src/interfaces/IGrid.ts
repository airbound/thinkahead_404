import { ICase } from './ICase';
export interface IGrid {
    lastCase: ICase | null | undefined
    cases: Array<Array<ICase>>
    isVertical: boolean;
}