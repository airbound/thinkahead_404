export interface IPlayer {
    nickname: string
    score: number,
    time: number
}