import { Grid } from '../entities/Grid';
import { Player } from '../entities/Player';
export interface IRoom {
    code: string;
    currentPlayer: Player | null
    turnsNumber: number
    players: Array<Player>
    grid: Grid
}