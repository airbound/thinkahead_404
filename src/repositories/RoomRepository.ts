import { Room } from "../entities/Room";
export class RoomRepository {
  private static rooms: Array<Room> = []
  static find(roomCode: string): Room | undefined {
    return RoomRepository.rooms.find(r => r.code === roomCode)
  }
  static add(room: Room) {
    RoomRepository.rooms = [...RoomRepository.rooms, room]
  }
}
