import "dotenv/config"
import {Server, Socket} from "socket.io"
import { RoomController } from './../controllers/RoomControllers';
import { Room } from '../entities/Room';
import { RoomRepository } from '../repositories/RoomRepository';

export class SocketService {
    private static io: Server;
    private static roomController: RoomController;
    private constructor(){};
    static bootstrap() {
        if(SocketService.io) {
            return;
        }
        SocketService.roomController = new RoomController();
        SocketService.io = require("socket.io")()
        SocketService.io.listen(process.env.SOCKET_PORT as any)
        SocketService.attachListeners();
    }
    private static attachListeners() {
        SocketService.io.sockets.on("connection", (socket: Socket) => {
           
            // on room create
            socket.on("room:create", async () => {
                const roomCode = await SocketService.roomController.create();
                if(roomCode) {
                    //join the socket room
                    socket.join(roomCode);
                    //send success
                    socket.emit("room:created", RoomRepository.find(roomCode));
                    return;
                }
            });

            //get roomdata
            socket.on("room:get", (roomCode) => {
                socket.emit("room", RoomRepository.find(roomCode))
            })

            // on player join
            socket.on("room:join", async ({roomCode, playerName}) => {
                try {
                    SocketService.roomController.join(playerName, roomCode);
                    socket.join(roomCode);
                    socket.emit("room:joined");
                    SocketService.sendRoomUpdate(roomCode)
                } catch (error) {
                    socket.emit("error")
                }
            });

            // on player play
            socket.on("room:play", async ({roomCode, playerName, caseId}) => {
                try {
                    SocketService.roomController.play(playerName, roomCode, caseId)
                    
                    if(SocketService.roomController.hasWinner(roomCode)) {
                        this.io.sockets.to(roomCode).emit("room:winner");
                    }
                    SocketService.sendRoomUpdate(roomCode)
                } catch (error) {
                    socket.emit("error")
                }
            });
        })
    }

    private static sendRoomUpdate(roomCode: string) {
        SocketService.io.sockets.to(roomCode).emit("room:update", RoomRepository.find(roomCode))
    }
}
