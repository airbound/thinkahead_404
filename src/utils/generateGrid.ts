import { ICase } from '../interfaces/ICase';
import { v4 } from 'uuid';
export const generateGrid = (): Array<Array<ICase>> => {
    let grid: Array<Array<ICase>> = []
    for (let i = 0; i < 10; i++) {
        grid[i] = []
        for (let y = 0; y < 10; y++) {
            const gridCase: ICase = {
                id: v4(),
                isTaken: false,
                number: Math.ceil(Math.random() * 10) * (Math.round(Math.random()) ? 1 : -1),
                x: i,
                y
            }
            grid[i] = [...grid[i], gridCase]
        }
    }
    return grid;
}